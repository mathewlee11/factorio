data:extend(
{
	{
        type = "equipment-grid",
		name = "mini-equipment-grid",
		width = 6,
		height = 6,
		equipment_categories = {"armor"}
	}
})


local fusion_robot_item = table.deepcopy(data.raw['item']['construction-robot'])
fusion_robot_item.name = "fusion-construction-robot"
fusion_robot_item.icon = "__FastestStart__/graphics/icons/fusion-construction-robot.png"
fusion_robot_item.place_result = "fusion-construction-robot"


local mini_fusion_reactor_item = table.deepcopy(data.raw['item']['fusion-reactor-equipment'])
mini_fusion_reactor_item.name = "mini-fusion-reactor-equipment"
mini_fusion_reactor_item.icon = nil
mini_fusion_reactor_item.icons = {{icon = "__base__/graphics/icons/fusion-reactor-equipment.png", tint = {r=0.53, g=0.81, b=0.92, a=1}} }
mini_fusion_reactor_item.placed_as_equipment_result = "mini-fusion-reactor-equipment"


local mini_armor = table.deepcopy(data.raw['armor']['modular-armor'])
mini_armor.name = "mini-power-armor"
mini_armor.icon = "__FastestStart__/graphics/icons/mini-power-armor.png"
mini_armor.resistances = {}
mini_armor.durability = 5000
mini_armor.equipment_grid = "mini-equipment-grid"


local mini_fusion_reactor = table.deepcopy(data.raw['generator-equipment']['fusion-reactor-equipment'])
mini_fusion_reactor.name = "mini-fusion-reactor-equipment"
mini_fusion_reactor.sprite.filename = "__FastestStart__/graphics/equipment/mini-fusion-reactor-equipment.png"
mini_fusion_reactor.sprite.width = 65
mini_fusion_reactor.shape.width = 2
mini_fusion_reactor.power = "1000kW"


local fusion_robot = table.deepcopy(data.raw['construction-robot']['construction-robot'])
fusion_robot.name = "fusion-construction-robot"
fusion_robot.icon = "__FastestStart__/graphics/icons/fusion-construction-robot.png"
fusion_robot.minable.result = "fusion-construction-robot"
fusion_robot.energy_per_tick = "0kJ"
fusion_robot.energy_per_move = "0kJ"
fusion_robot.speed = fusion_robot.speed * 5

data:extend({fusion_robot_item,mini_fusion_reactor_item,mini_armor,mini_fusion_reactor,fusion_robot})
