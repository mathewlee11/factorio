sudo apt-get install python3-pip
pip3 install google-api-python-client
pip3 install google-cloud-storage
wget https://www.factorio.com/get-download/0.14.22/headless/linux64
tar -xvf linux64
gcloud init
rm linux64
mkdir /home/ubuntu/saves
gsutil rsync gs://factorio-saves /home/ubuntu/saves
chmod 777 setup.sh
crontab saves.cron
